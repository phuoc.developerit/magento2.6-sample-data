<?php
namespace Freelance\PaymentFee\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;

class HolePunch extends AbstractFieldArray
{
    /**
     * Prepare field to render
     */
    protected function _prepareToRender()
    {
        $this->_addAfter = false;

        $this->addColumn(
            'from',
            [
                'label' => __('From')
            ]
        );
        $this->addColumn(
            'to',
            [
                'label' => __('To')
            ]
        );
        $this->addColumn(
            'price_fee',
            [
                'label' => __('Price')
            ]
        );
    }
}
