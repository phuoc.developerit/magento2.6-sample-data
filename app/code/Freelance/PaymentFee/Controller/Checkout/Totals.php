<?php
namespace Freelance\PaymentFee\Controller\Checkout;

use Magento\Framework\App\Action\Context;
use function GuzzleHttp\json_decode;

class Totals extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_resultJson;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Freelance\PaymentFee\Helper\Data
     */
    protected $dataHelper;
    public function __construct(
        Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Json\Helper\Data $helper,
        \Magento\Framework\Controller\Result\JsonFactory $resultJson,
        \Freelance\PaymentFee\Helper\Data $dataHelper
    )
    {
        parent::__construct($context);
        $this->_checkoutSession = $checkoutSession;
        $this->_helper = $helper;
        $this->_resultJson = $resultJson;
        $this->dataHelper = $dataHelper;
    }

    /**
     * Trigger to re-calculate the collect Totals
     *
     * @return bool
     */
    public function execute()
    {
        $response = [
            'errors' => false,
            'message' => 'Re-calculate successful.'
        ];

        try {
            $enabled = $this->dataHelper->isModuleEnabled();
            $quote = $this->_checkoutSession->getQuote();
            //Trigger to re-calculate totals
            $payment = $this->_helper->jsonDecode($this->getRequest()->getContent());
            $this->_checkoutSession->getQuote()->getPayment()->setMethod($payment['payment']);

            if ($enabled) {
                $fee = $this->dataHelper->calculatorPaymentFee(
                    $this->_checkoutSession->getQuote()
                );
                if ($fee) {
                    $quote->setFee($fee);
                } else {
                    $quote->setFee(NULL);
                }
            }

            $this->_checkoutSession->getQuote()->collectTotals()->save();

        } catch (\Exception $e) {
            $response = [
                'errors' => true,
                'message' => $e->getMessage()
            ];
        }

        /** @var \Magento\Framework\Controller\Result\Raw $resultRaw */
        $resultJson = $this->_resultJson->create();
        return $resultJson->setData($response);
    }
}