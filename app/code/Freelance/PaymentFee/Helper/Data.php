<?php

namespace Freelance\PaymentFee\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use function GuzzleHttp\json_decode;

class Data extends AbstractHelper
{
    const PAYMENT_ENABLE_ADD_FEE = [
        'onepay',
        'onepayinternational'
    ];
    /**
     * Custom fee config path
     */
    const CONFIG_CUSTOM_IS_ENABLED = 'paymentfee/paymentfee/status';
    const CONFIG_RULE_FEE = 'paymentfee/paymentfee/rule';
    const CONFIG_FEE_LABEL = 'paymentfee/paymentfee/name';
    const CONFIG_MINIMUM_ORDER_AMOUNT = 'paymentfee/paymentfee/minimum_order_amount';

    /**
     * @return mixed
     */
    public function isModuleEnabled()
    {

        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $isEnabled = $this->scopeConfig->getValue(self::CONFIG_CUSTOM_IS_ENABLED, $storeScope);
        return $isEnabled;
    }

    /**
     * Get custom fee
     *
     * @return mixed
     */
    public function getRuleFee()
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $fee = $this->scopeConfig->getValue(self::CONFIG_RULE_FEE, $storeScope);
        return $fee;
    }

    /**
     * Get custom fee
     *
     * @return mixed
     */
    public function getFeeLabel()
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $feeLabel = $this->scopeConfig->getValue(self::CONFIG_FEE_LABEL, $storeScope);
        return $feeLabel;
    }

    /**
     * @return mixed
     */
    public function getMinimumOrderAmount()
    {

        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $MinimumOrderAmount = $this->scopeConfig->getValue(self::CONFIG_MINIMUM_ORDER_AMOUNT, $storeScope);
        return $MinimumOrderAmount;
    }

    /**
     * @param $quote \Magento\Quote\Model\Quote
     * @return int|mixed
     */
    public function calculatorPaymentFee($quote)
    {
        $paymentMethod = $quote->getPayment()->getMethod();
        if (!in_array($paymentMethod, self::PAYMENT_ENABLE_ADD_FEE))
        {
            return 0;
        }
        $rules = $this->getRuleFee();

        $rules = json_decode($rules, true);
        $fee = 0;
        foreach ($rules as $rule) {

            if ($rule['from'] == null || $rule['to'] == null || $rule['price_fee'] == null ){
                continue;
            }
            $subtotal = $quote->getSubtotal();
            if ($subtotal >= $rule['from'] && $subtotal <= $rule['to']) {
                $fee = $rule['price_fee'];
            }
        }

        return $fee;
    }
}
