<?php
namespace Freelance\PaymentFee\Model;
use Magento\Checkout\Model\ConfigProviderInterface;
class PaymentFeeConfigProvider implements ConfigProviderInterface
{
    protected $dataHelper;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * PaymentFeeConfigProvider constructor.
     * @param \Freelance\PaymentFee\Helper\Data $dataHelper
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Freelance\PaymentFee\Helper\Data $dataHelper,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Psr\Log\LoggerInterface $logger

    )
    {
        $this->dataHelper = $dataHelper;
        $this->checkoutSession = $checkoutSession;
        $this->logger = $logger;
    }

    /**
     * @return array
     */
    public function getConfig()
    {

        ////// iyuu
        $customFeeConfig = [];
        $enabled = $this->dataHelper->isModuleEnabled();
        $customFeeConfig['fee_label'] = $this->dataHelper->getFeeLabel();
        $quote = $this->checkoutSession->getQuote();
        $subtotal = $quote->getSubtotal();
        $customFeeConfig['custom_fee_amount'] = 111;
        $customFeeConfig['show_hide_paymentfee_block'] = ($enabled && $quote->getFee()) ? true : false;
        $customFeeConfig['show_hide_paymentfee_shipblock'] = ($enabled) ? true : false;
        return $customFeeConfig;
    }
}