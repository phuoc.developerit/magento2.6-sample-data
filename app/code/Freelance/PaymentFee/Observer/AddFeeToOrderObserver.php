<?php
namespace Freelance\PaymentFee\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class AddFeeToOrderObserver implements ObserverInterface
{
    /**
     * Set payment fee to order
     *
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quote = $observer->getQuote();
        $PaymentFeeFee = $quote->getFee();
        $PaymentFeeBaseFee = $quote->getBaseFee();
        if (!$PaymentFeeFee || !$PaymentFeeBaseFee) {
            return $this;
        }
        //Set fee data to order
        $order = $observer->getOrder();
        $order->setData('fee', $PaymentFeeFee);
        $order->setData('base_fee', $PaymentFeeBaseFee);

        return $this;
    }
}
