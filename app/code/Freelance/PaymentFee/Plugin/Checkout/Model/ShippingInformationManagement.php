<?php
namespace Freelance\PaymentFee\Plugin\Checkout\Model;


use function GuzzleHttp\json_decode;
use function GuzzleHttp\json_encode;

class ShippingInformationManagement
{
    /**
     * @var \Magento\Quote\Model\QuoteRepository
     */
    protected $quoteRepository;

    /**
     * @var \Freelance\PaymentFee\Helper\Data
     */
    protected $dataHelper;

    /**
     * @param \Magento\Quote\Model\QuoteRepository $quoteRepository
     * @param \Freelance\PaymentFee\Helper\Data $dataHelper
     */
    public function __construct(
        \Magento\Quote\Model\QuoteRepository $quoteRepository,
        \Freelance\PaymentFee\Helper\Data $dataHelper
    )
    {
        $this->quoteRepository = $quoteRepository;
        $this->dataHelper = $dataHelper;
    }

    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     */
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    )
    {
        $enabled = $this->dataHelper->isModuleEnabled();
        if (!$enabled) {
            return [$cartId, $addressInformation];
        }
        $quote = $this->quoteRepository->getActive($cartId);

        $fee = $this->dataHelper->calculatorPaymentFee($quote);
        if ($fee) {
            $quote->setFee($fee);
        } else {
            $quote->setFee(NULL);
        }
    }
}

