var config = {
    "map": {
        "*": {
            'Magento_Checkout/js/model/shipping-save-processor/default': 'Freelance_PaymentFee/js/model/shipping-save-processor/default',
            'Magento_Checkout/js/action/select-payment-method': 'Freelance_PaymentFee/js/action/payment/select-payment-method'
        }
    }
};
