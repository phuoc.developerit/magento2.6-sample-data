<?php
namespace TTS\Improvedaddress\Block\Adminhtml\Carrier\Tablerate;

class Grid extends \Magento\OfflineShipping\Block\Adminhtml\Carrier\Tablerate\Grid
{
    protected function _prepareColumns()
    {
        $this->addColumn(
            'dest_village',
            ['header' => __('Village'), 'index' => 'dest_village', 'default' => null]
        );
        return parent::_prepareColumns();
    }
}