<?php
/**
 * @categroy Go Retail Company
 * @package Go Retail Company
 * @author Go Retail Company <sales@ttstech.com>
 * @date: 15/05/2017
 * @time: 10:18
 */

namespace TTS\Improvedaddress\Block\Adminhtml;


class City extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_blockGroup = 'TTS_Improvedaddress';
        $this->_controller = 'adminhtml_city';
        $this->_headerText = __('Cities Manager');
        $this->_addButtonLabel = __('Add New City');
        parent::_construct();
    }
}