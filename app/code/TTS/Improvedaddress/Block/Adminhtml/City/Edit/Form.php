<?php
/**
 * @categroy Go Retail Company
 * @package Go Retail Company
 * @author Go Retail Company <sales@ttstech.com>
 * @date: 15/05/2017
 * @time: 09:00
 */

namespace TTS\Improvedaddress\Block\Adminhtml\City\Edit;


class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Directory\Model\CountryFactory
     */
    protected $_countryRegionFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Directory\Model\CountryFactory $countryRegionFactory,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        $this->_countryRegionFactory = $countryRegionFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }
    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('city_form');
        $this->setTitle(__('City Information'));
    }
    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $form = $this->_formFactory->create(
            [
                'data' => [
                    'id' => 'edit_form',
                    'action' => $this->getData('action'),
                    'method' => 'post'
                ]
            ]
        );
        $form->setHtmlIdPrefix('city_');
        $formData = $this->_coreRegistry->registry('improvedaddress_city');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            [
                'legend' => __('General Information'),
                'class' => 'fieldset-wide'
            ]
        );
        $country_list = $this->_coreRegistry->registry('improvedaddress_city_country_list');
        $country = $fieldset->addField(
            'country_id',
            'select',
            [
                'name' => 'country_id',
                'label' => __('Country'),
                'title' => __('Country'),
                'required' => true,
                'values' => $country_list
            ]
        );

        if ($formData) {
            if ($formData->getRegionCode()) {
                $statearray = $this->_countryRegionFactory->create()->setId(
                    $formData->getCountryId()
                )->getLoadedRegionCollection()->toOptionArray();
            }else{
                $statearray = array();
            }
        }else{
            $statearray = array();
        }
        $region = $fieldset->addField(
            'region_id',
            'select',
            [
                'name' => 'region_id',
                'label' => __('State/Province'),
                'title' => __('State/Province'),
                'values' =>  $statearray,
                'required' => true
            ]
        );

        /**
         * Add Ajax to the Country select box html output
         */
        $message = '<div id="warning-message">'._("Please add State/Province for country before add City.").'</div>';
        $country->setAfterElementHtml("   
            <script type=\"text/javascript\">
                    require([
                    'jquery',
                    'mage/template',
                    'jquery/ui',
                    'mage/translate'
                ],
                function($, mageTemplate) {
                   $('#edit_form').on('change', '#city_country_id', function(event){
                        $.ajax({
                               url : '". $this->getUrl('*/*/regionlist')."',
                               type: 'post',
                               data: {country : $('#city_country_id').val()},
                               dataType: 'json',
                               showLoader:true,
                               success: function(data){
                                    if(!data.warning){
                                        $('#city_region_id').show();
                                        $('#warning-message').hide();
                                        $('#city_region_id').empty();
                                        $('#city_region_id').append(data.htmlconent);
                                    }else{
                                        $('#city_region_id').hide();
                                        $('#warning-message').show();
                                        $('#city_region_id').parent().append('".$message."');
                                    }
                               }
                        });
                   })
                }

            );
            </script>"
        );

        if(count($statearray) > 0){
            $region->setAfterElementHtml("   
                <script type=\"text/javascript\">
                        require([
                        'jquery',
                        'mage/template',
                        'jquery/ui',
                        'mage/translate'
                        ],
                            function($, mageTemplate) {
                                $('#city_region_id').val('".$formData->getRegionId()."');
                            }
                        );
                </script>
            ");
        }

        $fieldset->addField(
            'code',
            'text',
            [
                'name' => 'code',
                'label' => __('City Code'),
                'title' => __('City Code'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'default_name',
            'text',
            [
                'name' => 'default_name',
                'label' => __('City Name'),
                'title' => __('City Name'),
                'required' => true
            ]
        );

        if ($formData) {
            if ($formData->getCityId()) {
                $fieldset->addField(
                    'city_id',
                    'hidden',
                    ['name' => 'city_id']
                );
            }
            $form->setValues($formData->getData());
        }
        $form->setUseContainer(true);
        $form->setMethod('post');
        $this->setForm($form);
        return parent::_prepareForm();
    }
}