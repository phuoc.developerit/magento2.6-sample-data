<?php
/**
 * @categroy Go Retail Company
 * @package Go Retail Company
 * @author Go Retail Company <sales@ttstech.com>
 * @date: 22/05/2017
 * @time: 16:44
 */

namespace TTS\Improvedaddress\Block\Adminhtml\Edit\Renderer;


class City extends \Magento\Backend\Block\AbstractBlock implements
    \Magento\Framework\Data\Form\Element\Renderer\RendererInterface
{
    /**
     * @var \TTS\Improvedaddress\Helper\Data
     */
    protected $_helper;

    /**
     * @param \Magento\Backend\Block\Context $context
     * @param \TTS\Improvedaddress\Helper\Data $helper
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Context $context,
        \TTS\Improvedaddress\Helper\Data $helper,
        array $data = []
    ) {
        $this->_helper = $helper;
        parent::__construct($context, $data);
    }

    /**
     * Output the region element and javasctipt that makes it dependent from country element
     *
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function render(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $country = $element->getForm()->getElement('country_id');
        $region = $element->getForm()->getElement('region_id');

        $cityId = $element->getForm()->getElement('city')->getValue();
        $regiondefault = $element->getForm()->getElement('region_id')->getValue();

        $html = '<div class="field field-state required admin__field _required">';
        $element->setClass('input-text admin__control-text');
        $element->setRequired(true);
        $html .= $element->getLabelHtml() . '<div class="control admin__field-control">';
        $html .= $element->getElementHtml();

        $selectName = str_replace('city', 'city_id', $element->getName());
        $selectId = $element->getHtmlId() . '_id';
        $html .= '<select id="' .
            $selectId .
            '" name="' .
            $selectName .
            '" class="select required-entry admin__control-select" style="display:none">';
        $html .= '<option value="">' . __('Please select a city') . '</option>';
        $html .= '</select>';

        $html .= '<script>' . "\n";
        $html .= 'require(["prototype", "TTS_Improvedaddress/js/city-update"], function(){';
        $html .= '$("' . $selectId . '").setAttribute("defaultValue", "'. $cityId .'");' . "\n";
        $html .= 'new CitiesUpdater("' .
            $country->getHtmlId() .
            '", "' .
            $region->getHtmlId() .
            '", "' .
            $element->getHtmlId() .
            '", "' .
            $selectId .
            '", ' .
            $this->_helper->getCityJson() .
            ', "'.
            $regiondefault.
            '");' .
            "\n";

        $html .= '});';
        $html .= '</script>' . "\n";

        $html .= '</div></div>' . "\n";

        return $html;
    }
}