<?php
/**
 * @categroy Go Retail Company
 * @package Go Retail Company
 * @author Go Retail Company <sales@ttstech.com>
 * @date: 25/05/2017
 * @time: 08:19
 */

namespace TTS\Improvedaddress\Block\Adminhtml\Order\Address;


class Form extends \Magento\Sales\Block\Adminhtml\Order\Address\Form
{
    /**
     * Address form template
     *
     * @var string
     */
    protected $_template = 'Magento_Sales::order/address/form.phtml';

    /**
     * Return array of additional form element renderers by element id
     *
     * @return array
     */
    protected function _getAdditionalFormElementRenderers()
    {
        return [
            'region' => $this->getLayout()->createBlock('Magento\Customer\Block\Adminhtml\Edit\Renderer\Region'),
            'city' => $this->getLayout()->createBlock('TTS\Improvedaddress\Block\Adminhtml\Edit\Renderer\City')
        ];
    }
}