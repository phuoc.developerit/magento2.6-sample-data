<?php
/**
 * @categroy Go Retail Company
 * @package Go Retail Company
 * @author Go Retail Company <sales@ttstech.com>
 * @date: 25/05/2017
 * @time: 08:59
 */

namespace TTS\Improvedaddress\Block\Adminhtml\Order\Create\Billing;


class Address extends \Magento\Sales\Block\Adminhtml\Order\Create\Billing\Address
{
    /**
     * Return array of additional form element renderers by element id
     *
     * @return array
     */
    protected function _getAdditionalFormElementRenderers()
    {
        return [
            'region' => $this->getLayout()->createBlock('Magento\Customer\Block\Adminhtml\Edit\Renderer\Region'),
            'city' => $this->getLayout()->createBlock('TTS\Improvedaddress\Block\Adminhtml\Edit\Renderer\City')
        ];
    }
}