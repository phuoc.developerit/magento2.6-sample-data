<?php

namespace TTS\Improvedaddress\Block\LayoutProcessor\Checkout;

class Village implements \Magento\Checkout\Block\Checkout\LayoutProcessorInterface
{
    protected $villageCollection;
    public function __construct(
        \TTS\Improvedaddress\Model\ResourceModel\Village\CollectionFactory $villageCollection
    ) {
        $this->villageCollection = $villageCollection;
    }

    /**
     * @inheritDoc
     */
    public function process($jsLayout)
    {
        $customAttributeCode = 'village';
        $customField = [
            'component' => 'TTS_Improvedaddress/js/form/element/village',
            'config' => [
                // customScope is used to group elements within a single form (e.g. they can be validated separately)
                'customScope' => 'shippingAddress.custom_attributes',
                'customEntry' => 'shippingAddress.village',
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/select',
            ],
            'dataScope' => 'shippingAddress.custom_attributes' . '.' . $customAttributeCode,
            'label' => __('Village'),
            'provider' => 'checkoutProvider',
            'sortOrder' => 106,
            'validation' => [
                'required-entry' => true
            ],
            'filterBy' => [
                'target' => '${ $.provider }:shippingAddress.city',
                'field' => 'city_id',
            ],
            'options' => $this->villageCollection->create()->load()->toOptionArray(),
            'customEntry' => null,
            'visible' => true,
        ];
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children'][$customAttributeCode] = $customField;
        if (!isset($jsLayout['components']['checkoutProvider']['dictionaries']['village_id'])) {
            $jsLayout['components']['checkoutProvider']['dictionaries']['village_id'] =
                $this->villageCollection->create()->load()->toOptionArray();
        }
       // return $jsLayout;
        if (isset($jsLayout['components']['checkout']['children']['sidebar']['children']['shipping-information']['children']['ship-to'])) {
            $jsLayout['components']['checkout']['children']['sidebar']['children']['shipping-information']['children']['ship-to'] =
                [
                    'component' => 'Magento_Checkout/js/view/shipping-information/list',
                    'displayArea' => 'ship-to',
                    'rendererTemplates' => [
                        'customer-address' => [
                            'component' => 'TTS_Improvedaddress/js/view/checkout/shipping-information/address-renderer/shipping'
                        ],
                        'new-customer-address' => [
                            'component' => 'TTS_Improvedaddress/js/view/checkout/shipping-information/address-renderer/shipping'
                        ]
                    ]
                    ];
        }

        return $jsLayout;

    }

}