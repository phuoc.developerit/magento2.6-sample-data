<?php
namespace TTS\Improvedaddress\Console\Command;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\ResourceConnection;

class ImportVillage extends Command
{
    protected $villageCode = [];
    protected $appState;
    protected $csv;
    protected $moduleReader;

    public function __construct(
        State $appState,
        \Magento\Framework\File\Csv $csv,
        \Magento\Framework\Module\Dir\Reader $moduleReader,
        ResourceConnection $resourceConnection,
        $name = null)
    {
        parent::__construct($name);
        $this->appState = $appState;
        $this->csv = $csv;
        $this->moduleReader = $moduleReader;
        $this->resourceConnection = $resourceConnection;
    }

    protected function configure()
    {
        $this->setName('import:village')
            ->setDescription('Import Village address from Data folder of this module.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->appState->setAreaCode(Area::AREA_GLOBAL);
         $path = $this->moduleReader->getModuleDir(
            '',
            'TTS_Improvedaddress'
        );
        $fileImport = $path . '/' . 'Data/village_data.csv';

        $csvData = $this->csv->getData($fileImport);
        $i = 0;
        foreach ($csvData as $row => $data) {
            if ($i == 0){
                $connection  = $this->resourceConnection->getConnection();
                $villageTable = $connection->getTableName('directory_country_village');
                $sqlClearData = "DELETE FROM {$villageTable}";
                $connection->query($sqlClearData);
                print_r("Run clear data successfully. \n");
                $i++; continue;
            }
            $i++;
            if (count($data) == 4) {
                $countryId = $data[0];
                $regionId = $data[1];
                $cityId = $data[2];
                $villageName = $data[3];

                $code = $this->generalCode($cityId, $villageName);
                $insertSql = "INSERT INTO {$villageTable} (village_id, city_id, country_id, region_id, code, default_name)
VALUES ('{$i}','{$cityId}', '{$countryId}', '{$regionId}', '{$code}', '{$villageName}')";
                $connection->query($insertSql);
                print_r("Insert record: " . implode("-", $data). "\n");
            } else {
                print_r("Row #{$i} InValid \n");
            }
        }
    }

    public function generalCode($cityId, $villageName) {
        $codeCity = explode("-", $cityId);
        if (isset($codeCity[1])) {
            $firstCode = $codeCity[1];
        } else {
            $firstCode = $cityId;
        }

        $villageCode = $firstCode . '-' . $this->vn_to_str($villageName);
        if (!isset($this->villageCode[$villageCode])) {
            $this->villageCode[$villageCode] = 1;
            return $villageCode;
        }
        return $villageCode . random_int(1,10);
    }
    function vn_to_str ($str){
        $unicode = array(
            'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
            'd'=>'đ',
            'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
            'i'=>'í|ì|ỉ|ĩ|ị',
            'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
            'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
            'y'=>'ý|ỳ|ỷ|ỹ|ỵ',
            'A'=>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
            'D'=>'Đ',
            'E'=>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
            'I'=>'Í|Ì|Ỉ|Ĩ|Ị',
            'O'=>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
            'U'=>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
            'Y'=>'Ý|Ỳ|Ỷ|Ỹ|Ỵ',
        );
        foreach($unicode as $nonUnicode=>$uni){
            $str = preg_replace("/($uni)/i", $nonUnicode, $str);
        }
        $str = str_replace(' ','_',$str);
        $stringReplace = explode("_", $str);
        $shortName = '';
        foreach ($stringReplace as $text ){
            $shortName .= substr(trim($text), 0,1);
        }
        if (!$shortName) {
            return random_int(1,100);
        }
        return $shortName;
    }
}