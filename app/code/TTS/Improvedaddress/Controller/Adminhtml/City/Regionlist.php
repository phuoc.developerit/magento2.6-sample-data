<?php
/**
 * @categroy Go Retail Company
 * @package Go Retail Company
 * @author Go Retail Company <sales@ttstech.com>
 * @date: 21/04/2017
 * @time: 14:41
 */

namespace TTS\Improvedaddress\Controller\Adminhtml\City;

class Regionlist extends \Magento\Backend\App\Action
{

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var \Magento\Directory\Model\CountryFactory
     */
    protected $_countryFactory;

    /**
     * Regionlist constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    )
    {
        $this->_countryFactory = $countryFactory;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    /**
     * Default customer account page
     *
     * @return void
     */
    public function execute()
    {
        $countrycode = $this->getRequest()->getParam('country');
        $state = "";
        $result = array();
        if ($countrycode != '') {
            $statearray = $this->_countryFactory->create()->setId(
                $countrycode
            )->getLoadedRegionCollection()->toOptionArray();
            if(!empty($statearray)){
                $state .= "<option value=''>".__('Please select a region, state or province.')."</option>";
                foreach ($statearray as $_state) {
                    if($_state['value']){
                        $state .= "<option value='".$_state['value']."'>" . $_state['label'] . "</option>";
                    }
                }
                $result['htmlconent'] = $state;
            }else{
                $result['warning'] = true;
            }
        }

        $this->getResponse()->representJson(
            $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($result)
        );
    }
}