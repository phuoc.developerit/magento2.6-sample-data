<?php
/**
 * @categroy Go Retail Company
 * @package Go Retail Company
 * @author Go Retail Company <sales@ttstech.com>
 * @date: 17/05/2017
 * @time: 14:57
 */

namespace TTS\Improvedaddress\Controller\City;


use Magento\Framework\App\Action\Action;
use Magento\Framework\App\ResponseInterface;

class Index extends Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \TTS\Improvedaddress\Model\RegionFactory
     */
    protected $_regionFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \TTS\Improvedaddress\Model\RegionFactory $regionFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        $this->_regionFactory = $regionFactory;
        parent::__construct($context);
    }

    /**
     * Dispatch request
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $countrycode = $this->getRequest()->getParam('country');
        $regionId = $this->getRequest()->getParam('region_id');
        $city = "";
        $result = array();
        if ($countrycode != '') {
            $citiesarray = $this->_regionFactory->create()
                ->setData(
                    'country_code',
                    $countrycode
                )->setData(
                    'region_id',
                    $regionId
                )->getLoaderCityCollection()->toOptionArray();
            if(!empty($citiesarray)){
                $city .= "<option value=''>".__('Please select a region, state or province.')."</option>";
                foreach ($citiesarray as $_city) {
                    if($_city['value']){
                        $city .= "<option value='".$_city['value']."'>" . $_city['label'] . "</option>";
                    }
                }
                $result['htmlconent'] = $city;
            }else{
                $result['warning'] = true;
            }
        }

        $this->getResponse()->representJson(
            $this->_objectManager->get('Magento\Framework\Json\Helper\Data')->jsonEncode($result)
        );
    }
}