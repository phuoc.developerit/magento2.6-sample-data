<?php
namespace TTS\Improvedaddress\CustomerData;

class DirectoryData extends \Magento\Checkout\CustomerData\DirectoryData
{

    protected $cityCollection;
    protected $villageCollection;

    public function __construct(
        \Magento\Directory\Helper\Data $directoryHelper,
        \TTS\Improvedaddress\Model\ResourceModel\City\Collection $cityCollection,
        \TTS\Improvedaddress\Model\ResourceModel\Village\Collection $villageCollection
)
    {
        $this->cityCollection = $cityCollection;
        $this->villageCollection = $villageCollection;
        parent::__construct($directoryHelper);
    }

    /**
     * {@inheritdoc}
     */
    public function getSectionData()
    {
        $output = [];
        $regionsData = $this->directoryHelper->getRegionData();
        /**
         * @var string $code
         * @var \Magento\Directory\Model\Country $data
         */
        foreach ($this->directoryHelper->getCountryCollection() as $code => $data) {
            $output[$code]['name'] = $data->getName();
            if (array_key_exists($code, $regionsData)) {
                foreach ($regionsData[$code] as $key => $region) {
                    $output[$code]['regions'][$key]['code'] = $region['code'];
                    $output[$code]['regions'][$key]['name'] = $region['name'];
                }
            }

            $cityCollection = $this->getCityCollection($code);
            foreach ($cityCollection as $cityData) {
                $output[$code]['city'][$cityData['code']] = $cityData['default_name'];
            }
            $villageCollection = $this->villageCollection->addFieldToFilter('country_id', $code);
            foreach ($villageCollection as $villageData) {
                $output[$code]['village'][$villageData['code']] = $villageData['default_name'];
            }

        }
        return $output;
    }

    public function getCityCollection($countryCode) {
        $collection = $this->cityCollection->addFieldToFilter('country_id', $countryCode);
        return $collection;
    }
}