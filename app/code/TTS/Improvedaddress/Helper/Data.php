<?php
/**
 * @categroy Go Retail Company
 * @package Go Retail Company
 * @author Go Retail Company <sales@ttstech.com>
 * @date: 22/05/2017
 * @time: 11:55
 */

namespace TTS\Improvedaddress\Helper;


class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Json representation of cities data
     *
     * @var string
     */
    protected $_cityJson;

    /**
     * @var \Magento\Framework\App\Cache\Type\Config
     */
    protected $_configCacheType;

    /**
     * @var \TTS\Improvedaddress\Model\ResourceModel\City\CollectionFactory
     */
    protected $_cityCollectionFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonHelper;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\App\Cache\Type\Config $configCacheType
     * @param \Magento\Directory\Model\ResourceModel\Country\Collection $countryCollection
     * @param \Magento\Directory\Model\ResourceModel\Region\CollectionFactory $regCollectionFactory,
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Directory\Model\CurrencyFactory $currencyFactory
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\Cache\Type\Config $configCacheType,
        \TTS\Improvedaddress\Model\ResourceModel\City\CollectionFactory $cityCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Json\Helper\Data $jsonHelper
    ) {
        parent::__construct($context);
        $this->_configCacheType = $configCacheType;
        $this->_cityCollectionFactory = $cityCollectionFactory;
        $this->_storeManager = $storeManager;
        $this->jsonHelper = $jsonHelper;
    }

    /**
     * Retrieve cities data json
     *
     * @return string
     */
    public function getCityJson()
    {
        \Magento\Framework\Profiler::start('TEST: ' . __METHOD__, ['group' => 'TEST', 'method' => __METHOD__]);
        if (!$this->_cityJson) {
            $cacheKey = 'DIRECTORY_CITIES_JSON_STORE' . $this->_storeManager->getStore()->getId();
            $json = $this->_configCacheType->load($cacheKey);
            if (empty($json)) {
                $cities = $this->getCityData();
                $json = $this->jsonHelper->jsonEncode($cities);
                if ($json === false) {
                    $json = 'false';
                }
                $this->_configCacheType->save($json, $cacheKey);
            }
            $this->_cityJson = $json;
        }

        \Magento\Framework\Profiler::stop('TEST: ' . __METHOD__);
        return $this->_cityJson;
    }

    /**
     * Retrieve cities data
     *
     * @return array
     */
    public function getCityData()
    {
        $cities = array();

        $collection = $this->_cityCollectionFactory->create()->load();

        foreach ($collection as $item) {
            /** @var $item \TTS\Improvedaddress\Model\City */
            if (!$item->getId()) {
                continue;
            }
            $cities[$item->getCountryId()][$item->getRegionId()][$item->getId()] = [
                'code' => $item->getCode(),
                'name' => (string)__($item->getDefaultName()),
            ];
        }
        return $cities;
    }
}