<?php
/**
 * @categroy Go Retail Company
 * @package Go Retail Company
 * @author Go Retail Company <sales@ttstech.com>
 * @date: 15/05/2017
 * @time: 10:11
 */

namespace TTS\Improvedaddress\Model;

use Magento\Framework\Model\AbstractModel;

class City extends AbstractModel
{
    /**
     *  Cache Tag
     * @var string
     */
    const CACHE_TAG = 'directory_country_city';

    /**
     * City constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ){
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     *  Initialize resource model
     * @return void
     */
    protected function _construct()
    {
        $this->_init('TTS\Improvedaddress\Model\ResourceModel\City');
    }
}