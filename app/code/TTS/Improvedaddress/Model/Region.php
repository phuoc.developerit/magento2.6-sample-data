<?php
/**
 * @categroy Go Retail Company
 * @package Go Retail Company
 * @author Go Retail Company <sales@ttstech.com>
 * @date: 11/05/2017
 * @time: 14:24
 */

namespace TTS\Improvedaddress\Model;


use Magento\Framework\Model\AbstractModel;

class Region extends AbstractModel
{
    /**
     *  Cache Tag
     * @var string
     */
    const CACHE_TAG = 'directory_country_region';

    /**
     * @var ResourceModel\City\CollectionFactory
     */
    protected $_cityCollectionFactory;

    /**
     * City constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null $resourceCollection
     * @param ResourceModel\City\CollectionFactory $cityCollectionFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        ResourceModel\City\CollectionFactory $cityCollectionFactory,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ){
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->_cityCollectionFactory = $cityCollectionFactory;
    }

    /**
     *  Initialize resource model
     * @return void
     */
    protected function _construct()
    {
        $this->_init('TTS\Improvedaddress\Model\ResourceModel\Region');
    }

    /**
     * @return \TTS\Improvedaddress\Model\ResourceModel\City\Collection
     */
    public function getLoaderCityCollection()
    {
        $collection = $this->_cityCollectionFactory->create();
        $collection->addCountryFilter($this->getData('country_code'));
        $collection->addRegionFilter($this->getData('region_id'));
        return $collection;
    }
}