<?php
/**
 * @categroy Go Retail Company
 * @package Go Retail Company
 * @author Go Retail Company <sales@ttstech.com>
 * @date: 17/05/2017
 * @time: 15:17
 */

namespace TTS\Improvedaddress\Model;


class RegionFactory
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager)
    {
        $this->_objectManager = $objectManager;
    }

    /**
     * Create new city model
     *
     * @param array $arguments
     * @return \TTS\Improvedaddress\Model\Region
     */
    public function create(array $arguments = [])
    {
        return $this->_objectManager->create('TTS\Improvedaddress\Model\Region', $arguments);
    }
}