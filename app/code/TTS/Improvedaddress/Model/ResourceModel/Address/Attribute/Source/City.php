<?php
/**
 * @categroy Go Retail Company
 * @package Go Retail Company
 * @author Go Retail Company <sales@ttstech.com>
 * @date: 18/05/2017
 * @time: 12:05
 */

namespace TTS\Improvedaddress\Model\ResourceModel\Address\Attribute\Source;

class City extends \Magento\Eav\Model\Entity\Attribute\Source\Table
{
    /**
     * @var $_citiesFactory \TTS\Improvedaddress\Model\ResourceModel\City\CollectionFactory
     */
    protected $_citiesFactory;

    /**
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory $attrOptionCollectionFactory
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory $attrOptionFactory
     * @param \TTS\Improvedaddress\Model\ResourceModel\City\CollectionFactory $regionsFactory
     */
    public function __construct(
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory $attrOptionCollectionFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory $attrOptionFactory,
        \TTS\Improvedaddress\Model\ResourceModel\City\CollectionFactory $citiesFactory
    ) {
        $this->_citiesFactory = $citiesFactory;
        parent::__construct($attrOptionCollectionFactory, $attrOptionFactory);
    }

    /**
     * Retrieve all region options
     *
     * @return array
     */
    public function getAllOptions($withEmpty = true, $defaultValues = false)
    {
        if (!$this->_options) {
            $this->_options = $this->_createCitiesCollection()->load()->toOptionArray();
        }
        return $this->_options;
    }

    /**
     * @return \TTS\Improvedaddress\Model\ResourceModel\City\Collection
     */
    protected function _createCitiesCollection()
    {
        return $this->_citiesFactory->create();
    }
}
