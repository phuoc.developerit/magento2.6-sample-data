<?php

namespace TTS\Improvedaddress\Model\ResourceModel\Address\Attribute\Source;

class Village extends \Magento\Eav\Model\Entity\Attribute\Source\Table
{
    /**
     * @var $_villageFactory \TTS\Improvedaddress\Model\ResourceModel\City\CollectionFactory
     */
    protected $_villageFactory;

    /**
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory $attrOptionCollectionFactory
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory $attrOptionFactory
     * @param \TTS\Improvedaddress\Model\ResourceModel\City\CollectionFactory $regionsFactory
     */
    public function __construct(
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory $attrOptionCollectionFactory,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory $attrOptionFactory,
        \TTS\Improvedaddress\Model\ResourceModel\Village\CollectionFactory $villageFactory
    ) {
        $this->_villageFactory = $villageFactory;
        parent::__construct($attrOptionCollectionFactory, $attrOptionFactory);
    }

    /**
     * Retrieve all region options
     *
     * @return array
     */
    public function getAllOptions($withEmpty = true, $defaultValues = false)
    {
        if (!$this->_options) {
            $this->_options = $this->_createVillageCollection()->load()->toOptionArray();
        }
        return $this->_options;
    }

    /**
     * @return \TTS\Improvedaddress\Model\ResourceModel\City\Collection
     */
    protected function _createVillageCollection()
    {
        return $this->_villageFactory->create();
    }
}
