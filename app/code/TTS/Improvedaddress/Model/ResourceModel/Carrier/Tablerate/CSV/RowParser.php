<?php
namespace TTS\Improvedaddress\Model\ResourceModel\Carrier\Tablerate\CSV;

use Magento\OfflineShipping\Model\ResourceModel\Carrier\Tablerate\CSV\ColumnResolver;
use Magento\OfflineShipping\Model\ResourceModel\Carrier\Tablerate\CSV\RowException;

class RowParser
{
    /**
     * @param \Magento\OfflineShipping\Model\ResourceModel\Carrier\Tablerate\CSV\RowParser $subject
     * @param array $columns
     * @return array
     */
    public function afterGetColumns(\Magento\OfflineShipping\Model\ResourceModel\Carrier\Tablerate\CSV\RowParser $subject,
                                    array $columns)
    {
        $columns[] = 'dest_village';
        return $columns;
    }

    /**
     * @param \Magento\OfflineShipping\Model\ResourceModel\Carrier\Tablerate\CSV\RowParser $subject
     * @param \Closure $proceed
     * @param array $rowData
     * @param $rowNumber
     * @param $websiteId
     * @param $conditionShortName
     * @param $conditionFullName
     * @param ColumnResolver $columnResolver
     * @return mixed
     * @throws RowException
     * @throws \Magento\OfflineShipping\Model\ResourceModel\Carrier\Tablerate\CSV\ColumnNotFoundException
     */
    public function aroundParse(
        \Magento\OfflineShipping\Model\ResourceModel\Carrier\Tablerate\CSV\RowParser $subject,
        \Closure $proceed,
        array $rowData,
        $rowNumber,
        $websiteId,
        $conditionShortName,
        $conditionFullName,
        ColumnResolver $columnResolver
    ) {

        // validate row
        if (count($rowData) < 6) {
            throw new RowException(
                __(
                    'The Table Rates File Format is incorrect in row number "%1". Please export and re-import.',
                    $rowNumber
                )
            );
        }
        $village = $this->getVillage( $rowData, $columnResolver);
        $rates = $proceed($rowData,
            $rowNumber,
            $websiteId,
            $conditionShortName,
            $conditionFullName,
            $columnResolver);

        foreach ($rates as &$rate) {
            $rate['village'] = $village;
        }
        return $rates;
    }

    /**
     * @param array $rowData
     * @param ColumnResolver $columnResolver
     * @return float|int|string|null
     * @throws \Magento\OfflineShipping\Model\ResourceModel\Carrier\Tablerate\CSV\ColumnNotFoundException
     */
    private function getVillage(array $rowData, ColumnResolver $columnResolver)
    {
        $village = $columnResolver->getColumnValue("Village", $rowData);
        if ($village === '') {
            $village = '*';
        }

        return $village;
    }
}