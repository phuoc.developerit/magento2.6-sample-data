<?php
namespace TTS\Improvedaddress\Model\ResourceModel\Carrier\Tablerate;

class DataHashGenerator extends \Magento\OfflineShipping\Model\ResourceModel\Carrier\Tablerate\DataHashGenerator
{
    /**
     * @param array $data
     * @return string
     */
    public function getHash(array $data)
    {
        $village = $data['village'] ? $data['village'] : 0;
        $countryId = $data['dest_country_id'];
        $regionId = $data['dest_region_id'];
        $zipCode = $data['dest_zip'];
        $conditionValue = $data['condition_value'];

        return sprintf("%s-%s-%d-%s-%F", $village, $countryId, $regionId, $zipCode, $conditionValue);
    }
}