<?php
/**
 * @categroy Go Retail Company
 * @package Go Retail Company
 * @author Go Retail Company <sales@ttstech.com>
 * @date: 15/05/2017
 * @time: 10:12
 */

namespace TTS\Improvedaddress\Model\ResourceModel;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\AbstractModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class City extends AbstractDb
{

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('directory_country_city', 'city_id');
    }

    /**
     * @param AbstractModel $object
     */
    protected function _beforeSave(AbstractModel $object)
    {
        if (!$this->checkCityAlreadyExist($object)) {
            throw new LocalizedException(
                __('The city already exists.')
            );
        }
    }

    /**
     * @param AbstractModel $object
     * @param $field
     * @return bool
     */
    protected function checkSubCityAlreadyExist(AbstractModel $object, $field)
    {
        $select = $this->getConnection()->select()
            ->from(['dcr' => $this->getMainTable()])
            ->where('dcr.country_id = ?', $object->getData('country_id'))
            ->where('dcr.region_id = ?', $object->getData('region_id'))
            ->where('dcr.'.$field.' = ?', $object->getData($field));
        if ($object->getData('city_id')) {
            $select->where('dcr.city_id <> ?', $object->getData('city_id'));
        }
        if ($this->getConnection()->fetchRow($select)) {
            return false;
        }
        return true;
    }

    /**
     * @param AbstractModel $object
     * @return bool
     */
    protected function checkCityAlreadyExist(AbstractModel $object)
    {
        if (!$this->checkSubCityAlreadyExist($object, 'default_name') || !$this->checkSubCityAlreadyExist($object, 'code')) {
            return false;
        }
        return true;
    }
}