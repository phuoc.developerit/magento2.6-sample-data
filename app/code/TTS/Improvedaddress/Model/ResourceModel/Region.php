<?php
/**
 * @categroy Go Retail Company
 * @package Go Retail Company
 * @author Go Retail Company <sales@ttstech.com>
 * @date: 11/05/2017
 * @time: 14:27
 */

namespace TTS\Improvedaddress\Model\ResourceModel;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\AbstractModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Region extends AbstractDb
{

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('directory_country_region', 'region_id');
    }

    /**
     * @param AbstractModel $object
     */
    protected function _beforeSave(AbstractModel $object)
    {
        if (!$this->checkRegionAlreadyExist($object)) {
            throw new LocalizedException(
                __('The region already exists.')
            );
        }
    }

    /**
     * @param AbstractModel $object
     * @param $field
     * @return bool
     */
    protected function checkSubRegionAlreadyExist(AbstractModel $object, $field)
    {
        $select = $this->getConnection()->select()
            ->from(['dcr' => $this->getMainTable()])
            ->where('dcr.country_id = ?', $object->getData('country_id'))
            ->where('dcr.'.$field.' = ?', $object->getData($field));
        if ($object->getData('region_id')) {
            $select->where('dcr.region_id <> ?', $object->getData('region_id'));
        }
        if ($this->getConnection()->fetchRow($select)) {
            return false;
        }
        return true;
    }

    /**
     * @param AbstractModel $object
     * @return bool
     */
    protected function checkRegionAlreadyExist(AbstractModel $object)
    {
        if (!$this->checkSubRegionAlreadyExist($object, 'default_name') || !$this->checkSubRegionAlreadyExist($object, 'code')) {
            return false;
        }
        return true;
    }
}