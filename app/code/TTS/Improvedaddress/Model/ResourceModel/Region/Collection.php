<?php
/**
 * @categroy Go Retail Company
 * @package Go Retail Company
 * @author Go Retail Company <sales@ttstech.com>
 * @date: 11/05/2017
 * @time: 14:35
 */

namespace TTS\Improvedaddress\Model\ResourceModel\Region;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'region_id';

    /**
     *  Define collection resource model
     */
    protected function _construct()
    {
        $this->_init(
            'TTS\Improvedaddress\Model\Region',
            'TTS\Improvedaddress\Model\ResourceModel\Region'
        );
    }
}