<?php
/**
 * @categroy TTS
 * @package  TTS
 * @author Nguyen Tien Dung<nguyentiendungktpm@gmail.com>
 * @date: 17/04/2017
 * @time: 14:28
 */

namespace TTS\Improvedaddress\Setup;


use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if ($installer->tableExists('directory_country_region')) {
            $installer->getConnection()->addIndex(
                $installer->getTable('directory_country_region'),
                $setup->getIdxName(
                    $installer->getTable('directory_country_region'),
                    ['default_name'],
                    \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
                ),
                ['default_name'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
            );
        }
        $installer->endSetup();
    }
}