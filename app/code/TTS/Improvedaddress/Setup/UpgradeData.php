<?php
/**
 * @categroy Sutunam
 * @package Sutunam
 * @author Nguyen Tien Dung<nguyentiendung@sutunam.com>
 * @date: 18/05/2017
 * @time: 11:53
 */

namespace TTS\Improvedaddress\Setup;


use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Indexer\IndexerRegistry;
use Magento\Customer\Model\Customer;
use Magento\Customer\Setup\CustomerSetupFactory;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * Customer setup factory
     *
     * @var CustomerSetupFactory
     */
    protected $customerSetupFactory;

    /**
     * @var IndexerRegistry
     */
    protected $indexerRegistry;

    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $eavConfig;

    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        IndexerRegistry $indexerRegistry,
        \Magento\Eav\Model\Config $eavConfig
    ){
        $this->customerSetupFactory = $customerSetupFactory;
        $this->indexerRegistry = $indexerRegistry;
        $this->eavConfig = $eavConfig;
    }

    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        if (version_compare($context->getVersion(), '1.0.4', '<')) {
            /** @var \Magento\Customer\Setup\CustomerSetup $customerSetup */
            $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
//            $customerSetup->updateAttribute(
//                'customer_address',
//                'city',
//                'source_model',
//                'TTS\Improvedaddress\Model\ResourceModel\Address\Attribute\Source\City'
//            );
            $indexer = $this->indexerRegistry->get(Customer::CUSTOMER_GRID_INDEXER_ID);
            $indexer->reindexAll();
            $this->eavConfig->clear();
        }

        if (version_compare($context->getVersion(), '2.0.0', '<')) {
            /** @var \Magento\Customer\Setup\CustomerSetup $customerSetup */
            $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);

            $customerSetup->addAttribute(
                Customer::ENTITY,
                'village',
                [
                    'type' => 'varchar',
                    'label' => 'village',
                    'input' => 'text',
                    'required' => true,
                    'sort_order' => 87,
                    'visible' => true,
                    'system' => true,
                    'source_model' => ''
                ]
            );

            $attribute = $customerSetup->getEavConfig()->getAttribute('customer_address', 'village')
                ->addData(['used_in_forms' => [
                    'customer_address_edit',
                    'customer_register_address'
                ]]);
            $attribute->save();

            $indexer = $this->indexerRegistry->get(Customer::CUSTOMER_GRID_INDEXER_ID);
            $indexer->reindexAll();
            $this->eavConfig->clear();
        }

        $setup->endSetup();
    }
}