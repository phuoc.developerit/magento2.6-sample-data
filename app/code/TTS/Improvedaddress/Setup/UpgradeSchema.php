<?php
/**
 * @categroy Sutunam
 * @package Sutunam
 * @author Nguyen Tien Dung<nguyentiendung@sutunam.com>
 * @date: 15/05/2017
 * @time: 10:06
 */

namespace TTS\Improvedaddress\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Customer\Setup\CustomerSetupFactory;
class UpgradeSchema implements UpgradeSchemaInterface
{
    private $customerSetupFactory;
    public function __construct(CustomerSetupFactory $customerSetupFactory)
    {
        $this->customerSetupFactory = $customerSetupFactory;
    }

    /**
     * Upgrades DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if (version_compare($context->getVersion(), '1.0.4', '<')) {
            $table = $installer->getConnection()->newTable($installer->getTable('directory_country_city'));
            $table->addColumn(
                'city_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'City Id'
            )->addColumn(
                'country_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                4,
                ['nullable' => false, 'default' => '0'],
                'Country Id in ISO-2'
            )->addColumn(
                'region_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => true, 'default' => 0],
                'Region Id'
            )->addColumn(
                'region_code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                32,
                ['nullable' => true],
                'Region Code'
            )
                ->addColumn(
                    'code',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    64,
                    ['nullable' => true, 'default' => null],
                    'City code'
                )->addColumn(
                    'default_name',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    [],
                    'City Name'
                )->addIndex(
                    $installer->getIdxName('directory_country_city', ['country_id']),
                    ['country_id']
                )->setComment(
                    'Directory Country City'
                );
            $installer->getConnection()->createTable($table);
            /**
             * Create table 'directory_country_city_name'
             */
            $table = $installer->getConnection()->newTable(
                $installer->getTable('directory_country_city_name')
            )->addColumn(
                'locale',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                8,
                ['nullable' => false, 'primary' => true, 'default' => false],
                'Locale'
            )->addColumn(
                'city_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false, 'primary' => true, 'default' => '0'],
                'City Id'
            )->addColumn(
                'name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => true, 'default' => null],
                'City Name'
            )->addIndex(
                $installer->getIdxName('directory_country_city_name', ['city_id']),
                ['city_id']
            )->addForeignKey(
                $installer->getFkName(
                    'directory_country_city_name',
                    'city_id',
                    'directory_country_city',
                    'city_id'
                ),
                'city_id',
                $installer->getTable('directory_country_city'),
                'city_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )->setComment(
                'Directory Country City Name'
            );
            $installer->getConnection()->createTable($table);
        }

        if (version_compare($context->getVersion(), '2.0.0', '<')) {
            $table = $installer->getConnection()->newTable($installer->getTable('directory_country_village'));
            $table->addColumn(
                'village_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Village Id'
            )->addColumn(
                'country_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                4,
                ['nullable' => false, 'default' => 'VN'],
                'Country Id in ISO-2'
            )->addColumn(
                'region_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => true],
                'Region Id'
            )->addColumn(
                'city_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['unsigned' => true, 'nullable' => true],
                'City Id'
            )->addColumn(
                'code',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                32,
                ['nullable' => true],
                'Village Code'
            )
            ->addColumn(
                'default_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                [],
                'Village Name'
            )->setComment(
                'Directory Country Village'
            );
            $installer->getConnection()->createTable($table);
        }

        if (version_compare($context->getVersion(), '2.0.1', '<')) {
            $attributeCode = 'village';

            $installer->getConnection()->addColumn(
                $installer->getTable('quote_address'),
                $attributeCode,
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 32,
                    'nullable' => false,
                    'default' => null,
                    'comment' => 'Village column'
                ]
            );

            $installer->getConnection()->addColumn(
                $installer->getTable('sales_order_address'),
                $attributeCode,
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 32,
                    'nullable' => false,
                    'default' => null,
                    'comment' => 'Village column'
                ]
            );
        }

        if (version_compare($context->getVersion(), '2.0.2', '<')) {
            $this->addNewColumnVillageToTableRate($setup);
        }
        if (version_compare($context->getVersion(), '2.0.3', '<')) {
            $this->modifyIndexTableRate($setup);
        }
        $installer->endSetup();
    }

    /**
     * @param $setup SchemaSetupInterface
     */
    public function modifyIndexTableRate($setup) {
        $setup->getConnection()->dropIndex(
            $setup->getTable('shipping_tablerate'),
            $setup->getIdxName('shipping_tablerate',
                ['website_id', 'dest_country_id', 'dest_region_id', 'dest_zip', 'condition_name', 'condition_value', 'dest_village'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                )
        );
        // run custom sql
        $sql = 'ALTER TABLE '. $setup->getTable('shipping_tablerate'). ' CHANGE COLUMN `dest_village` `dest_village` varchar(10) NULL DEFAULT "*"';
        $setup->getConnection()->query($sql);


        $sqlUpdate = 'UPDATE '. $setup->getTable('shipping_tablerate'). ' set dest_village = "*"';
        $setup->getConnection()->query($sqlUpdate);

        $indexSql = "ALTER TABLE `". $setup->getTable('shipping_tablerate') . "` 
ADD UNIQUE INDEX `UNQ_05C18DD2A3D6D22E05D2C1E07CDAC9D5`(`website_id`, `dest_country_id`, `dest_region_id`, `dest_zip`, `condition_name`, `condition_value`, `dest_village`) USING BTREE";
        $setup->getConnection()->query($indexSql);
    }

    /**
     * @param $setup SchemaSetupInterface
     */
    public function addNewColumnVillageToTableRate($setup)
    {
        $setup->getConnection()->addColumn(
            $setup->getTable('shipping_tablerate'),
            'dest_village',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'default' => null,
                'size' => 10,
                'comment' => 'Village',
                'after' => 'dest_region_id'
            ]
        );
    }
}