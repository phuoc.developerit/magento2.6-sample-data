define([
    "prototype",
    "mage/adminhtml/events"
], function() {
    CitiesUpdater = Class.create();
    CitiesUpdater.prototype = {
        initialize: function (countryEl, regionSelectEl, cityTextEl, citySelectEl, cities, regiondefault, disableAction, clearRegionValueOnDisable) {
            this.countryEl = $(countryEl);
            this.regionSelectEl = $(regionSelectEl);
            this.cityTextEl = $(cityTextEl);
            this.citySelectEl = $(citySelectEl);
            this.cities = cities;
            this.regiondefault = regiondefault;
            this.disableAction = (typeof disableAction == 'undefined') ? 'hide' : disableAction;
            this.clearRegionValueOnDisable = (typeof clearRegionValueOnDisable == 'undefined') ? false : clearRegionValueOnDisable;

            if (this.citySelectEl.options.length <= 1) {
                this.update(true);
            } else {
                this.lastRegionId = this.regionSelectEl.value;
            }

            this.regionSelectEl.changeUpdater = this.update.bind(this);

            Event.observe(this.regionSelectEl, 'change', this.update.bind(this));

            Event.observe(this.citySelectEl, 'change', function (el) {
                $(cityTextEl).value = $(el.target).value;
            });
        },

        update: function (flag) {
            if(flag == true && this.cities[this.countryEl.value] && this.cities[this.countryEl.value][this.regiondefault]){
                var i, option, city, def;

                def = this.citySelectEl.getAttribute('defaultValue').toLowerCase();
                if (this.cityTextEl) {
                    if (!def) {
                        def = this.cityTextEl.value.toLowerCase();
                    }
                    this.cityTextEl.value = '';
                }

                this.citySelectEl.options.length = 1;
                var regionID = this.regiondefault;
                for (cityId in this.cities[this.countryEl.value][regionID]) {
                    city = this.cities[this.countryEl.value][regionID][cityId];

                    option = document.createElement('OPTION');
                    option.value = city.name.stripTags();
                    option.text = city.name.stripTags();
                    option.title = city.name;

                    if (this.citySelectEl.options.add) {
                        this.citySelectEl.options.add(option);
                    } else {
                        this.citySelectEl.appendChild(option);
                    }

                    if (cityId == def || city.name.toLowerCase() == def || city.code.toLowerCase() == def) {
                        this.citySelectEl.value = city.name.stripTags();
                        this.cityTextEl.value = city.name.stripTags();
                    }
                }

                if (this.disableAction == 'hide') {
                    if (this.cityTextEl) {
                        this.cityTextEl.style.display = 'none';
                        this.cityTextEl.style.disabled = true;
                    }
                    this.citySelectEl.style.display = '';
                    this.citySelectEl.disabled = false;
                } else if (this.disableAction == 'disable') {
                    if (this.cityTextEl) {
                        this.cityTextEl.disabled = true;
                    }
                    this.citySelectEl.disabled = false;
                }
                this.setMarkDisplay(this.citySelectEl, true);

                this.lastRegionId = this.regionSelectEl.value;
            }else if (this.cities[this.countryEl.value][this.regionSelectEl.value]) {
                if(this.lastRegionId != this.regionSelectEl.value) {
                    var i, option, city, def;

                    def = this.citySelectEl.getAttribute('defaultValue').toLowerCase();
                    if (this.cityTextEl) {
                        if (!def) {
                            def = this.cityTextEl.value.toLowerCase();
                        }
                        this.cityTextEl.value = '';
                    }

                    this.citySelectEl.options.length = 1;
                    var regionID = this.regionSelectEl.value;
                    for (cityId in this.cities[this.countryEl.value][regionID]) {
                        city = this.cities[this.countryEl.value][regionID][cityId];

                        option = document.createElement('OPTION');
                        option.value = city.name.stripTags();
                        option.text = city.name.stripTags();
                        option.title = city.name;

                        if (this.citySelectEl.options.add) {
                            this.citySelectEl.options.add(option);
                        } else {
                            this.citySelectEl.appendChild(option);
                        }

                        if (cityId == def || city.name.toLowerCase() == def || city.code.toLowerCase() == def) {
                            this.citySelectEl.value = city.name.stripTags();
                            this.cityTextEl.value = city.name.stripTags();
                        }
                    }
                }

                if (this.disableAction == 'hide') {
                    if (this.cityTextEl) {
                        this.cityTextEl.style.display = 'none';
                        this.cityTextEl.style.disabled = true;
                    }
                    this.citySelectEl.style.display = '';
                    this.citySelectEl.disabled = false;
                } else if (this.disableAction == 'disable') {
                    if (this.cityTextEl) {
                        this.cityTextEl.disabled = true;
                    }
                    this.citySelectEl.disabled = false;
                }
                this.setMarkDisplay(this.citySelectEl, true);

                this.lastRegionId = this.regionSelectEl.value;
            } else {
                if (this.disableAction == 'hide') {
                    if (this.cityTextEl) {
                        this.cityTextEl.style.display = '';
                        this.cityTextEl.style.disabled = false;
                    }
                    this.citySelectEl.style.display = 'none';
                    this.citySelectEl.disabled = true;
                } else if (this.disableAction == 'disable') {
                    if (this.cityTextEl) {
                        this.cityTextEl.disabled = false;
                    }
                    this.citySelectEl.disabled = true;
                    if (this.clearRegionValueOnDisable) {
                        this.citySelectEl.value = '';
                    }
                } else if (this.disableAction == 'nullify') {
                    this.citySelectEl.options.length = 1;
                    this.citySelectEl.value = '';
                    this.citySelectEl.selectedIndex = 0;
                    this.lastRegionId = '';
                }
                this.setMarkDisplay(this.citySelectEl, false);
            }
        },

        setMarkDisplay: function (elem, display) {
            if (elem.parentNode.parentNode) {
                var marks = Element.select(elem.parentNode.parentNode, '.required');
                if (marks[0]) {
                    display ? marks[0].show() : marks[0].hide();
                }
            }
        }
    };
});