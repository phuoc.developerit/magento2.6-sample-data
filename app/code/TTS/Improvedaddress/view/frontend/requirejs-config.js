/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            cityUpdater:            'TTS_Improvedaddress/js/city-updater'
        }
    },
    config: {
        mixins: {
            'Magento_Checkout/js/action/set-billing-address': {
                'TTS_Improvedaddress/js/action/set-billing-address-mixin': true
            },
            'Magento_Checkout/js/action/set-shipping-information': {
                'TTS_Improvedaddress/js/action/set-shipping-information-mixin': true
            },
            'Magento_Checkout/js/action/create-shipping-address': {
                'TTS_Improvedaddress/js/action/create-shipping-address-mixin': true
            },
            'Magento_Checkout/js/action/place-order': {
                'TTS_Improvedaddress/js/action/set-billing-address-mixin': true
            },
            'Magento_Checkout/js/action/create-billing-address': {
                'TTS_Improvedaddress/js/action/set-billing-address-mixin': true
            }
        }
    }
};
