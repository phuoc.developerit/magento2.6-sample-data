define(['Magento_Ui/js/form/element/select'], function (Select) {
    'use strict';
    return Select.extend({
        defaults: {
            skipValidation: false,
            imports: {
                update: '${ $.parentName }.city:value',
                initialOptions: "index = checkoutProvider:dictionaries.village_id",
                setOptions: "index = checkoutProvider:dictionaries.village_id"
            }
        },
    });
});