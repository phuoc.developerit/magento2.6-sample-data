define(
    [],
    function () {
        "use strict";
        return {
            getRules: function() {
                return {
                    'country_id': {
                        'required': true
                    },
                    'postcode': {
                        'required': true
                    },
                    'region_id': {
                        'required': true
                    },
                    'region_id_input': {
                        'required': true
                    },
                    'telephone': {
                        'required': true
                    },
                    'village_input': {
                        'required': true
                    },
                    'village': {
                        'required': true
                    },
                    'city': {
                        'required': true
                    },
                    'city_id': {
                        'required': true
                    }
                };
            }
        };
    }
);