/**
 * Refer to LICENSE.txt distributed with the Temando Shipping module for notice of license
 */

define([
    'underscore',
    'uiComponent',
    'Magento_Customer/js/customer-data',
], function (_, Component, customerData) {
    'use strict';

    var countryData = customerData.get('directory-data');

    return Component.extend({
        defaults: {
            template: 'TTS_Improvedaddress/shipping-information/address-renderer/shipping-render'
        },
        /**
         * @param {*} countryId
         * @return {String}
         */
        getCountryName: function (countryId) {
            return countryData()[countryId] != undefined ? countryData()[countryId].name : ''; //eslint-disable-line
        },
        getTemplate: function () {
            return this.template;
        },
        getCityLabel: function (countryId, code) {
            if (countryData()[countryId] != undefined && countryData()[countryId]['city'][code] != undefined) {
                return countryData()[countryId]['city'][code];
            }
            return code;
        },
        getVillageLabel: function (countryId, code) {
            if (countryData()[countryId] != undefined && countryData()[countryId]['village'][code] != undefined) {
                return countryData()[countryId]['village'][code];
            }
            return code;
        }
    });
});
